package net.nnemesis.forest;

public interface InputListener {
    public abstract void onInput(String input, int clientID);
}
