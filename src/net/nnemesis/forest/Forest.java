package net.nnemesis.forest;

import java.util.Scanner;

public class Forest {
    public static void main(String[] args) throws Exception {
        System.out.println("Forest v0.1 Server Starting...");

        AcceptThread at = new AcceptThread();
        at.start();

        Scanner in = new Scanner(System.in);

        System.out.print("> ");

        while (in.hasNextLine()) {
            System.out.print("> ");
            Command c = new Command(in.nextLine());

            if (c.getCmd() == Command.UNKNOWN) {
                System.out.println("(Cli) Unknown command");
                continue;
            }

            if (c.getCmd() == Command.SYS_STOP) {
                at.kill();

                System.exit(0);
            }

            if (c.getCmd() == Command.NET_DISCONNECT) {
                if (c.getArg(0).equals("all")) {

                } else {

                }

                continue;
            }
        }
    }
}
