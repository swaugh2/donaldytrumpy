package net.nnemesis.forest;

import java.net.ServerSocket;
import java.net.Socket;

public class AcceptThread extends Thread {
    private boolean running = true;

    private int currentID = 0;

    public void run() {
        try {
            ServerSocket ss = new ServerSocket(23);

            while (running) {
                try {
                    Socket cs = ss.accept();
                    new ClientThread(cs, ++currentID).start();
                } catch (Exception e) {
                    System.out.println("(Net) Client droppped: " + e.toString());
                    continue;
                }
            }
        } catch (Exception e) {
            System.out.println("(Net) Failed to open server socket: " + e.toString());
        }
    }

    public void kill() {
        running = false;
    }
}
